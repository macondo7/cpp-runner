// Definition for singly-linked list.
struct ListNode {
  int val;
  ListNode* next;
  ListNode() : val(0), next(nullptr) {}
  ListNode(int x) : val(x), next(nullptr) {}
  ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
 public:
  ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
    ListNode dummy = ListNode(-1);
    ListNode* p = &dummy;
    int mod = 0;
    while (l1 || l2 || mod != 0) {
      int x1 = l1 ? l1->val : 0;
      int x2 = l2 ? l2->val : 0;
      p->next = new ListNode((x1 + x2 + mod) % 10);
      p = p->next;

      mod = (x1 + x2 + mod) / 10;
      l1 = l1 ? l1->next : nullptr;
      l2 = l2 ? l2->next : nullptr;
    }
    return dummy.next;
  }
};