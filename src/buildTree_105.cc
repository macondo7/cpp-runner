#include <iostream>
#include <vector>

using namespace std;

// Definition for a binary tree node.
struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode() : val(0), left(nullptr), right(nullptr) {}
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
  TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
  TreeNode *construct(vector<int> &preorder, int pl, int pr, vector<int> &inorder, int il, int ir) {
    if (pl > pr) return nullptr;
    TreeNode *root = new TreeNode(preorder[pl]);
    int occInorder = il;
    for (int k = il; k <= ir; k++) {
      if (inorder[k] == preorder[pl]) {
        occInorder = k;
      }
    }
    int lLen = occInorder - il;
    root->left = construct(preorder, pl + 1, pl + lLen, inorder, il, occInorder - 1);
    root->right = construct(preorder, pl + lLen + 1, pr, inorder, occInorder + 1, ir);
    return root;
  }

 public:
  TreeNode *buildTree(vector<int> &preorder, vector<int> &inorder) {
    int n = preorder.size();
    return construct(preorder, 0, n - 1, inorder, 0, n - 1);
  }
};