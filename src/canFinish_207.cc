#include <algorithm>
#include <iostream>
#include <queue>
#include <vector>
using namespace std;

class Solution {
 public:
  bool canFinish(int numCourses, vector<vector<int>>& prerequisites) {
    vector<vector<int>> g = vector<vector<int>>(numCourses, vector<int>(numCourses));
    vector<int> indeg = vector<int>(numCourses, 0);
    for (int i = 0; i < prerequisites.size(); i++) {
      int a = prerequisites[i][0], b = prerequisites[i][1];
      g[b][a] = 1;
      indeg[a] += 1;
    }
    // bfs operate
    queue<int> q;
    for (int i = 0; i < numCourses; i++) {
      if (indeg[i] == 0) q.push(i);
    }
    int visited = 0;
    while (!q.empty()) {
      int head = q.front();
      q.pop();
      visited += 1;
      for (int i = 0; i < numCourses; i++) {
        if (!g[head][i]) continue;
        indeg[i] -= 1;
        if (indeg[i] == 0) {
          q.push(i);
        }
      }
    }
    return visited == numCourses;
  }
};