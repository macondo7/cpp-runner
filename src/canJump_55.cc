#include <iostream>
#include <vector>

using namespace std;

// 直觉的动态规划会超时，建议直接使用贪心的思想
class Solution {
 public:
  bool canJump(vector<int>& nums) {
    int n = nums.size(), mx = 0;
    for (int i = 0; i < n; i++) {
      if (mx < i) return false;
      mx = max(mx, i + nums[i]);
    }
    return true;
  }
};

int main() {
  vector<int> nums = {2, 3, 1, 1, 4};
  Solution c;
  cout << boolalpha << c.canJump(nums) << endl;
  nums = {3, 2, 1, 0, 4};
  cout << boolalpha << c.canJump(nums) << endl;
}