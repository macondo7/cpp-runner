#include <iostream>
#include <vector>

using namespace std;

class Solution {
 public:
  bool canPartition(vector<int>& nums) {
    int n = nums.size(), sum = 0;
    for (int i = 0; i < n; i++) {
      sum += nums[i];
    }
    if (sum % 2 == 1) return false;
    int m = sum / 2;
    vector<vector<bool>> dp(n + 1, vector<bool>(m + 1, false));
    dp[0][0] = true;
    // dp op
    for (int i = 1; i <= n; i++) {
      for (int j = 0; j <= m; j++) {
        dp[i][j] = dp[i - 1][j] || (j >= nums[i - 1] && dp[i - 1][j - nums[i - 1]]);
      }
    }
    return dp[n][m];
  }
};