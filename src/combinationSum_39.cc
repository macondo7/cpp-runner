#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

class Solution {
  int target;
  vector<vector<int>> result;

 public:
  void backtrack(vector<int>& candidates, vector<int> inner, int curSum, int idx) {
    if (curSum > this->target) return;
    if (curSum == this->target) {
      this->result.push_back(inner);
      return;
    };
    for (int i = idx; i < candidates.size(); i++) {
      inner.push_back(candidates[i]);
      backtrack(candidates, inner, curSum + candidates[i], i);
      inner.pop_back();
    }
  }

  vector<vector<int>> combinationSum(vector<int>& candidates, int target) {
    this->result = {};
    this->target = target;
    backtrack(candidates, {}, 0, 0);
    return result;
  }
};

int main() {
  Solution c;
  vector<int> candidates = {2, 3, 6, 7};
  vector<vector<int>> result = c.combinationSum(candidates, 7);
  cout << result.size() << endl;
  for (vector<int> v : result) {
    for (int val : v) {
      cout << val << ' ';
    }
    cout << endl;
  }
}