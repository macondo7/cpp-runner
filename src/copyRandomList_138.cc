#include <cstddef>
#include <unordered_map>

using namespace std;

// Definition for a Node.
class Node {
 public:
  int val;
  Node* next;
  Node* random;

  Node(int _val) {
    val = _val;
    next = NULL;
    random = NULL;
  }
};

class Solution {
 public:
  Node* copyRandomList(Node* head) {
    // first duplicate linked lsit
    Node dummy = Node(-1);
    Node* p = &dummy;
    unordered_map<Node*, Node*> coorRelation;
    for (Node* cur = head; cur; cur = cur->next) {
      p->next = new Node(cur->val);
      p = p->next;
      coorRelation[cur] = p;
    }
    // second linked random pointer
    p = dummy.next;
    for (Node* cur = head; cur; cur = cur->next) {
      p->random = coorRelation[cur->random];
      p = p->next;
    }
    return dummy.next;
  }
};