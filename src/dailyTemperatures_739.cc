#include <stack>
#include <vector>

using namespace std;

class Solution {
 public:
  vector<int> dailyTemperatures(vector<int>& temperatures) {
    vector<int> answer = vector<int>(temperatures.size(), 0);
    stack<int> nextTemp;
    for (int i = temperatures.size() - 1; i >= 0; i--) {
      while (!nextTemp.empty() && temperatures[nextTemp.top()] <= temperatures[i]) {
        nextTemp.pop();
      }
      if (!nextTemp.empty()) answer[i] = nextTemp.top() - i;
      nextTemp.push(i);
    }
    return answer;
  }
};