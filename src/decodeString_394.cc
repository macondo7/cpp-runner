#include <iostream>
#include <stack>
#include <vector>

using namespace std;

class Solution {
 public:
  string decodeString(string s) {
    string res = "";
    stack<int> kq;
    stack<string> preq;

    int k = 0;
    for (char c : s) {
      if (c >= '0' && c <= '9') {
        k = k * 10 + (c - '0');
      } else if (c == '[') {
        kq.push(k);
        preq.push(res);
        k = 0, res = "";
      } else if (c == ']') {
        string tmp;
        for (int i = 0; i < kq.top(); i++) {
          tmp += res;
        }
        kq.pop();
        res = preq.top() + tmp;
        preq.pop();
      } else {
        res += c;
      }
    }
    return res;
  }
};