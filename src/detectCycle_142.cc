// Definition for singly-linked list.
struct ListNode {
  int val;
  ListNode *next;
  ListNode(int x) : val(x), next(nullptr) {}
};

class Solution {
 public:
  ListNode *detectCycle(ListNode *head) {
    ListNode *fast = head, *slow = head;
    ListNode *boom = nullptr;
    while (fast && fast->next) {
      fast = fast->next->next;
      slow = slow->next;
      if (fast == slow) {
        boom = fast;
        break;
      }
    }
    if (!boom) return nullptr;
    fast = head;
    while (boom != fast) {
      fast = fast->next;
      boom = boom->next;
    }
    return boom;
  }
};
