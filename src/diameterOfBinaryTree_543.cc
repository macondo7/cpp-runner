#include <algorithm>

using namespace std;

// Definition for a binary tree node.
struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode() : val(0), left(nullptr), right(nullptr) {}
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
  TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
  int maxDIameter = 0;

 public:
  int postTraversal(TreeNode *p) {
    if (!p) return 0;
    int ld = postTraversal(p->left);
    int rd = postTraversal(p->right);
    maxDIameter = max(ld + rd, maxDIameter);
    return max(ld, rd) + 1;
  }

  int diameterOfBinaryTree(TreeNode *root) {
    postTraversal(root);
    return maxDIameter;
  }
};