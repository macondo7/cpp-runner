#include <string>
#include <vector>

using namespace std;

class Solution {
  int m, n, strLen;
  string word;

 public:
  bool dfs(vector<vector<char>>& board, int x, int y, int cur) {
    if (cur == strLen) return true;
    if (x >= m || x < 0 || y >= n || y < 0 || board[x][y] != word[cur]) return false;

    char tmp = board[x][y];
    board[x][y] = '*';
    // Are you kiding me? Why for range couldn't pass all tests.
    if (dfs(board, x + 1, y, cur + 1) || dfs(board, x - 1, y, cur + 1)) return true;
    if (dfs(board, x, y + 1, cur + 1) || dfs(board, x, y - 1, cur + 1)) return true;
    board[x][y] = tmp;
    return false;
  }

  bool exist(vector<vector<char>>& board, string word) {
    m = board.size(), n = board[0].size(), strLen = word.size();
    this->word = word;
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        if (dfs(board, i, j, 0)) {
          return true;
        }
      }
    }
    return false;
  }
};