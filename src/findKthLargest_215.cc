#include <iostream>
#include <vector>

using namespace std;

class Solution {
 public:
  int partition(vector<int>& nums, int l, int r, int k) {
    if (r == l) return nums[r];
    int i = l - 1, j = r + 1, t = nums[(l + r) >> 1];
    while (i < j) {
      while (nums[++i] < t);
      while (nums[--j] > t);
      if (i < j) swap(nums[i], nums[j]);
    }
    if (j < k) return partition(nums, j + 1, r, k);
    return partition(nums, l, j, k);
  }

  int findKthLargest(vector<int>& nums, int k) {
    int n = nums.size(), x = nums.size() - k;
    return partition(nums, 0, n - 1, x);
  }
};

int main() {
  vector<int> nums = {3, 2, 1, 5, 6, 4};
  Solution s;
  cout << s.findKthLargest(nums, 2) << endl;
  cout << s.findKthLargest(nums, 1) << endl;
  nums = {3, 1, 1, 1, 2, 1};
  cout << s.findKthLargest(nums, 2) << endl;
  cout << s.findKthLargest(nums, 1) << endl;
}