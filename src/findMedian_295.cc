#include <iostream>
#include <queue>

using namespace std;

/**
 * Your MedianFinder object will be instantiated and called as such:
 * MedianFinder* obj = new MedianFinder();
 * obj->addNum(num);
 * double param_2 = obj->findMedian();
 */
class MedianFinder {
  priority_queue<int> left;
  priority_queue<int, vector<int>, greater<int>> right;

 public:
  MedianFinder() {}

  void addNum(int num) {
    left.push(num);
    right.push(left.top());
    left.pop();

    if (left.size() < right.size()) {
      left.push(right.top());
      right.pop();
    }
  }

  double findMedian() {
    int l = left.size(), r = right.size();
    return l > r ? left.top() : (left.top() + right.top()) / 2.0;
  }
};
