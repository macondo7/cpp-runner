// Definition for a binary tree node.
struct TreeNode {
  int val;
  TreeNode* left;
  TreeNode* right;
  TreeNode() : val(0), left(nullptr), right(nullptr) {}
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
  TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};

class Solution {
 public:
  TreeNode* traversal(TreeNode* p) {
    if (!p) return p;
    TreeNode* l = traversal(p->left);
    TreeNode* r = traversal(p->right);
    p->left = nullptr;
    p->right = l;

    TreeNode* x = p;
    while (x->right) {
      x = x->right;
    }
    x->right = r;
    return p;
  }

  void flatten(TreeNode* root) { traversal(root); }
};