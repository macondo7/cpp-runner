#include <iostream>
#include <vector>

using namespace std;

class Solution {
  vector<string> result;
  int n;

  void backtrack(string tmp, int lnum, int rnum) {
    if (lnum == rnum && rnum == n) {
      result.push_back(tmp);
      return;
    }
    if (lnum > n || rnum > n) return;
    backtrack(tmp + '(', lnum + 1, rnum);
    if (rnum < lnum) backtrack(tmp + ')', lnum, rnum + 1);
  }

 public:
  vector<string> generateParenthesis(int n) {
    this->n = n;
    backtrack("", 0, 0);
    return result;
  }
};