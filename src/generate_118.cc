#include <iostream>
#include <vector>

using namespace std;

class Solution {
 public:
  vector<vector<int>> generate(int numRows) {
    vector<vector<int>> result;
    for (int level = 0; level < numRows; level++) {
      vector<int> inner(level + 1, 1);
      for (int i = 1; i < level; i++) {
        inner[i] = result[level - 1][i - 1] + result[level - 1][i];
      }
      result.push_back(inner);
    }
    return result;
  }
};