#include <stack>
#include <vector>

using namespace std;

// Definition for a binary tree node.
struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode() : val(0), left(nullptr), right(nullptr) {}
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
  TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
 public:
  vector<int> inorderTraversal(TreeNode *root) {
    vector<int> result;
    stack<TreeNode *> inorder;
    while (!inorder.empty() || root) {
      while (root) {
        inorder.push(root);
        root = root->left;
      }
      TreeNode *visited = inorder.top();
      inorder.pop();
      result.push_back(visited->val);
      root = visited->right;
    }
    return result;
  }
};