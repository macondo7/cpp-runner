// Definition for a binary tree node.
#include <iostream>
struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode() : val(0), left(nullptr), right(nullptr) {}
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
  TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
 public:
  bool traversal(TreeNode *root, int64_t maxValue, int64_t minValue) {
    if (!root) return true;
    if (root->val <= minValue || root->val >= maxValue) return false;
    bool l = traversal(root->left, root->val, minValue);
    bool r = traversal(root->right, maxValue, root->val);
    return l && r;
  }

  bool isValidBST(TreeNode *root) {
    int64_t maxValue = 1LL << 31;
    int64_t minValue = -maxValue - 1;
    return traversal(root, maxValue, minValue);
  }
};

int main() {
  std::cout << "Size of long: " << sizeof(long) << " bytes" << std::endl;
  std::cout << "Range of long: " << LONG_MIN << " to " << LONG_MAX << std::endl;
  int64_t maxValue = 1LL << 31;
  int64_t minValue = -maxValue - 1;
  std::cout << maxValue << ' ' << minValue << std::endl;
  return 0;
}