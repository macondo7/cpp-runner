#include <iostream>
#include <stack>

using namespace std;

class Solution {
 public:
  bool isValid(string s) {
    stack<char> sk;
    char maps[128] = {'0'};
    maps[')'] = '(';
    maps['}'] = '{';
    maps[']'] = '[';

    for (int i = 0; i < s.length(); i++) {
      if (s[i] == '(' || s[i] == '[' || s[i] == '{') {
        sk.push(s[i]);
      } else {
        if (sk.empty() || sk.top() != maps[s[i]]) {
          return false;
        }
        sk.pop();
      }
    }
    return sk.empty();
  }
};