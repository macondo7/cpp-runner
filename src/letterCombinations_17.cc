#include <iostream>
#include <vector>

using namespace std;

class Solution {
  vector<string> charset;
  vector<string> result;

  void backtrack(string& digits, int start, string tmp) {
    if (start == digits.size()) {
      result.push_back(tmp);
      return;
    }
    string subset = charset[digits[start] - '2'];
    for (char c : subset) {
      backtrack(digits, start + 1, tmp + c);
    }
  }

 public:
  vector<string> letterCombinations(string digits) {
    if (digits.empty()) return result;
    charset = {"abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
    backtrack(digits, 0, "");
    return result;
  }
};