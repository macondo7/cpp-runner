#include <queue>
#include <vector>

using namespace std;

// Definition for a binary tree node.
struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode() : val(0), left(nullptr), right(nullptr) {}
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
  TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
 public:
  vector<vector<int>> levelOrder(TreeNode *root) {
    vector<vector<int>> result;
    if (!root) return result;
    queue<TreeNode *> levelQueue;
    levelQueue.push(root);
    int cnt = 1;

    while (!levelQueue.empty()) {
      vector<int> inner;
      int nextCnt = 0;
      for (int i = 0; i < cnt; i++) {
        TreeNode *head = levelQueue.front();
        levelQueue.pop();
        inner.push_back(head->val);
        if (head->left) {
          levelQueue.push(head->left);
          nextCnt++;
        }
        if (head->right) {
          levelQueue.push(head->right);
          nextCnt++;
        }
      }
      cnt = nextCnt;
      result.push_back(inner);
    }
    return result;
  }
};