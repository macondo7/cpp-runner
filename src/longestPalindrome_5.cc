#include <iostream>
#include <vector>

using namespace std;

class Solution {
 public:
  string longestPalindrome(string str) {
    int m = str.size(), ans = 1, pos = 0;
    vector<vector<bool>> dp(m, vector<bool>(m));
    for (int e = 0; e < m; e++) {
      for (int s = 0; s <= e; s++) {
        dp[s][e] = str[s] == str[e] && (e - s < 2 || dp[s + 1][e - 1]);
        if (!dp[s][e]) continue;
        if (e - s + 1 > ans) {
          ans = e - s + 1;
          pos = s;
        }
      }
    }
    return str.substr(pos, ans);
  }
};