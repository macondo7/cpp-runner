#include <iostream>
#include <vector>

using namespace std;

class Solution {
 public:
  int longestValidParentheses(string s) {
    int n = s.size(), ans = 0;
    vector<int> dp(n);
    for (int i = 1; i < n; i++) {
      char cur = s[i], pre = s[i - 1];
      if (cur == ')' && pre == '(') {
        int preLen = i - 2 >= 0 ? dp[i - 2] : 0;
        dp[i] = preLen + 2;
      } else if (cur == ')') {
        int coorIdx = i - dp[i - 1] - 1;
        if (coorIdx >= 0 && s[coorIdx] == '(') {
          dp[i] = dp[i - 1] + 2;
          if (coorIdx > 0) {
            dp[i] += dp[coorIdx - 1];
          }
        }
      }
      ans = max(ans, dp[i]);
    }
    return ans;
  }
};