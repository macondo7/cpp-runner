#include <list>
#include <unordered_map>

using namespace std;

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache* obj = new LRUCache(capacity);
 * int param_1 = obj->get(key);
 * obj->put(key,value);
 */
class LRUCache {
  struct Node {
    int key;
    int val;
  };
  list<Node> values;
  unordered_map<int, list<Node>::iterator> hashMap;
  int capacity;

 public:
  LRUCache(int capacity) { this->capacity = capacity; }

  int moveToFronted(int key) {
    list<Node>::iterator iter = hashMap[key];
    Node node = *iter;
    values.erase(iter);
    values.push_front(node);
    hashMap[key] = values.begin();
    return node.val;
  }

  int get(int key) {
    if (!hashMap.count(key)) return -1;
    return moveToFronted(key);
  }

  void put(int key, int value) {
    if (hashMap.count(key)) {
      moveToFronted(key);
      hashMap[key]->val = value;
      return;
    }
    if (values.size() == capacity) {
      Node deletedNode = values.back();
      values.pop_back();
      hashMap.erase(deletedNode.key);
    }
    values.push_front(Node{key : key, val : value});
    hashMap[key] = values.begin();
  }
};
