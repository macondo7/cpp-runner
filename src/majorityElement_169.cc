#include <iostream>
#include <vector>

using namespace std;

class Solution {
 public:
  int majorityElement(vector<int>& nums) {
    int ans = nums[0], occur = 1;
    for (int i = 1; i < nums.size(); i++) {
      if (occur == 0) {
        ans = nums[i];
      }
      if (ans == nums[i]) {
        occur++;
      } else {
        occur--;
      }
    }
    return ans;
  }
};