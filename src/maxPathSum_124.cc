#include <iostream>

using namespace std;

// Definition for a binary tree node.
struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode() : val(0), left(nullptr), right(nullptr) {}
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
  TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
 public:
  int traversal(TreeNode *root, int &result) {
    if (!root) return 0;
    int l = traversal(root->left, result);
    int r = traversal(root->right, result);
    result = max(result, l + r + root->val);
    return max(0, root->val + max(max(l, r), 0));
  }

  int maxPathSum(TreeNode *root) {
    int result = INT32_MIN;
    traversal(root, result);
    return result;
  }
};