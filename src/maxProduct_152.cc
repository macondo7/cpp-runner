#include <vector>

using namespace std;

class Solution {
 public:
  bool isFlow(int cur, int multi) {
    long logicNum = (long)cur * multi;
    return logicNum > INT32_MAX || logicNum < INT32_MIN;
  }

  int maxProduct(vector<int>& nums) {
    int preMin = nums[0], preMax = nums[0];
    int res = nums[0];
    for (int i = 1; i < nums.size(); i++) {
      int cur = nums[i];
      int logicMax = isFlow(cur, preMax) ? cur : cur * preMax;
      int logicMin = isFlow(cur, preMin) ? cur : cur * preMin;
      preMin = min(cur, min(logicMax, logicMin));
      preMax = max(cur, max(logicMax, logicMin));
      res = max(res, preMax);
    }
    return res;
  }
};