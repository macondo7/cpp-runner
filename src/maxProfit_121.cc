#include <iostream>
#include <vector>

using namespace std;

class Solution {
 public:
  int maxProfit(vector<int>& prices) {
    int lmin = INT32_MAX, profit = 0;
    for (int i = 0; i < prices.size(); i++) {
      lmin = min(lmin, prices[i]);
      profit = max(profit, prices[i] - lmin);
    }
    return profit;
  }
};