#include <iostream>
#include <queue>
#include <stack>
#include <vector>

using namespace std;

// Definition for singly-linked list.
struct ListNode {
  int val;
  ListNode* next;
  ListNode() : val(0), next(nullptr) {}
  ListNode(int x) : val(x), next(nullptr) {}
  ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
 public:
  ListNode* mergeKLists(vector<ListNode*>& lists) {
    struct customLess {
      bool operator()(ListNode* a, ListNode* b) { return a->val > b->val; }
    };
    priority_queue<ListNode*, vector<ListNode*>, customLess> pq;
    for (ListNode* node : lists) {
      if (node) pq.push(node);
    }
    ListNode dummy = ListNode(-1);
    ListNode* p = &dummy;

    while (!pq.empty()) {
      p->next = pq.top();
      p = p->next;
      pq.pop();
      if (p->next) pq.push(p->next);
    }
    return dummy.next;
  }
};

int main() {
  ListNode* head = new ListNode(1);
  head->next = new ListNode(4);
  head->next->next = new ListNode(5);
  ListNode* head2 = new ListNode(1);
  head2->next = new ListNode(3);
  head2->next->next = new ListNode(4);
  ListNode* head3 = new ListNode(2);
  head3->next = new ListNode(6);
  vector<ListNode*> lists = {head, head2, head3};
  Solution s;
  ListNode* sortedList = s.mergeKLists(lists);
  while (sortedList) {
    cout << sortedList->val << ' ';
    sortedList = sortedList->next;
  }
  cout << endl;
}