#include <iostream>
#include <stack>

using namespace std;

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack* obj = new MinStack();
 * obj->push(val);
 * obj->pop();
 * int param_3 = obj->top();
 * int param_4 = obj->getMin();
 */
class MinStack {
  stack<int> minStack;
  stack<int> store;

 public:
  MinStack() {}

  void push(int val) {
    store.push(val);
    if (minStack.empty() || minStack.top() >= val) {
      minStack.push(val);
    }
  }

  void pop() {
    int top = store.top();
    store.pop();
    if (minStack.top() == top) {
      minStack.pop();
    }
  }

  int top() { return store.top(); }

  int getMin() { return minStack.top(); }
};
