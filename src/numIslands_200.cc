#include <vector>

using namespace std;

class Solution {
  int m, n;

 public:
  void dfs(vector<vector<char>>& grid, int x, int y) {
    if (x < 0 || x >= m || y < 0 || y >= n || grid[x][y] != '1') return;
    grid[x][y] = '0';
    dfs(grid, x + 1, y);
    dfs(grid, x - 1, y);
    dfs(grid, x, y + 1);
    dfs(grid, x, y - 1);
  }
  int numIslands(vector<vector<char>>& grid) {
    m = grid.size(), n = grid[0].size();
    int result = 0;
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        if (grid[i][j] == '1') {
          dfs(grid, i, j);
          result += 1;
        }
      }
    }
    return result;
  }
};