#include <iostream>
#include <vector>

using namespace std;

/**
 * 动态规划，直觉转移方程：dp[i][j] = min(dp[i-1][j], dp[i][j-i*i]+1)
 */
class Solution {
 public:
  int numSquares(int n) {
    int m = sqrt(n);
    vector<vector<int>> dp(m + 1, vector<int>(n + 1, INT_MAX));
    dp[0][0] = 0;
    for (int i = 1; i <= m; i++) {
      for (int j = 0; j <= n; j++) {
        dp[i][j] = dp[i - 1][j];
        if (j >= i * i) {
          dp[i][j] = min(dp[i][j], dp[i][j - i * i] + 1);
        }
      }
    }
    return dp[m][n];
  }
};