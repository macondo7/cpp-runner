#include <iostream>
#include <queue>
#include <vector>

using namespace std;

class Solution {
 public:
  int orangesRotting(vector<vector<int>>& grid) {
    queue<pair<int, int>> rottings;
    int fresh = 0, m = grid.size(), n = grid[0].size();
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        if (grid[i][j] == 1) fresh++;
        if (grid[i][j] == 2) rottings.emplace(i, j);
      }
    }

    int result = 0;
    int dir[4][2] = {{-1, 0}, {1, 0}, {0, 1}, {0, -1}};
    while (!rottings.empty() && fresh) {
      result++;
      for (int i = rottings.size(); i > 0; i--) {
        pair<int, int> coors = rottings.front();
        int x = coors.first, y = coors.second;
        rottings.pop();
        for (int p = 0; p < 4; p++) {
          int newX = dir[p][0] + x, newY = dir[p][1] + y;
          if (newX >= 0 && newX < m && newY >= 0 && newY < n && grid[newX][newY] == 1) {
            fresh--;
            grid[newX][newY] = 2;
            rottings.emplace(newX, newY);
          }
        }
      }
    }
    return fresh ? -1 : result;
  }
};

int main() {
  vector<vector<int>> grid = {{1, 2, 0}};
  Solution c;
  cout << c.orangesRotting(grid) << endl;
  grid = {{2, 1, 1}, {1, 1, 0}, {0, 1, 1}};
  cout << c.orangesRotting(grid) << endl;
}