#include <iostream>
#include <vector>

using namespace std;

class Solution {
 public:
  vector<int> partitionLabels(string s) {
    vector<int> index(128, 0);
    for (int i = 0; i < s.size(); i++) {
      index[s[i]] = i;
    }
    vector<int> result;
    for (int i = 0; i < s.size();) {
      int j = i;
      int mx = index[s[j]];
      while (j < mx) {
        j++;
        mx = max(mx, index[s[j]]);
      }
      result.push_back(j - i + 1);
      i = j + 1;
    }
    return result;
  }
};