#include <iostream>
#include <vector>

using namespace std;

class Solution {
  vector<vector<bool>> palindrome;
  vector<vector<string>> result;
  int n;

 public:
  void backtrack(string& s, int vi, vector<string> inner) {
    if (vi == n) {
      result.push_back(inner);
      return;
    }
    for (int i = vi; i < n; i++) {
      if (palindrome[vi][i]) {
        inner.push_back(s.substr(vi, i - vi + 1));
        backtrack(s, i + 1, inner);
        inner.pop_back();
      }
    }
  }

  vector<vector<string>> partition(string s) {
    // is palindrome logic
    n = s.size();
    palindrome = vector<vector<bool>>(n, vector<bool>(n, false));
    for (int end = 0; end < n; end++) {
      for (int start = 0; start <= end; start++) {
        if (s[start] == s[end] && (end - start < 2 || palindrome[start + 1][end - 1])) {
          palindrome[start][end] = true;
        }
      }
    }
    // backtrack obtains result
    backtrack(s, 0, {});
    return result;
  }
};