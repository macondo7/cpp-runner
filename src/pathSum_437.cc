#include <iostream>
#include <unordered_map>

using namespace std;

// Definition for a binary tree node.
struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode() : val(0), left(nullptr), right(nullptr) {}
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
  TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

// Fix: s as a sum, maybe overflow, so use `long` instead.
class Solution {
  unordered_map<long, int> cnt;
  int targetSum;

 public:
  int traversal(TreeNode *root, long s) {
    if (!root) return 0;
    s += root->val;
    int result = cnt[s - targetSum];
    cnt[s] += 1;
    result += traversal(root->left, s) + traversal(root->right, s);
    cnt[s] -= 1;
    return result;
  }

  int pathSum(TreeNode *root, int targetSum) {
    cnt[0] = 1;
    this->targetSum = targetSum;
    return traversal(root, 0);
  }
};