#include <iostream>
#include <vector>

using namespace std;

class Solution {
  vector<vector<int>> result;
  vector<bool> visited;

 public:
  void backtrack(vector<int>& nums, vector<int>& tmp) {
    if (tmp.size() == nums.size()) {
      result.push_back(tmp);
      return;
    }
    for (int i = 0; i < nums.size(); i++) {
      if (visited[i]) continue;
      tmp.push_back(nums[i]);
      visited[i] = true;
      backtrack(nums, tmp);
      visited[i] = false;
      tmp.pop_back();
    }
  }

  vector<vector<int>> permute(vector<int>& nums) {
    visited = vector<bool>(nums.size(), false);
    vector<int> tmp;
    backtrack(nums, tmp);
    return result;
  }
};