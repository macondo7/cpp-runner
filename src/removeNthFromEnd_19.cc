
// Definition for singly-linked list.
struct ListNode {
  int val;
  ListNode* next;
  ListNode() : val(0), next(nullptr) {}
  ListNode(int x) : val(x), next(nullptr) {}
  ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
 public:
  ListNode* removeNthFromEnd(ListNode* head, int n) {
    ListNode dummy = ListNode(-1, head);
    ListNode *fast = &dummy, *pre = &dummy;
    for (int i = 0; i < n; i++) {
      fast = fast->next;
    }
    while (fast && fast->next) {
      fast = fast->next, pre = pre->next;
    }
    pre->next = pre->next->next;
    return dummy.next;
  }
};