// Definition for singly-linked list.
struct ListNode {
  int val;
  ListNode* next;
  ListNode() : val(0), next(nullptr) {}
  ListNode(int x) : val(x), next(nullptr) {}
  ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
 public:
  ListNode* reverseKGroup(ListNode* head, int k) {
    int curLen = 0;
    ListNode* p = head;
    while (p) {
      p = p->next;
      curLen++;
    }
    if (curLen < k) return head;

    p = head;
    for (int i = 0; i < k - 1; i++) {
      p = p->next;
    }
    ListNode* nGroup = reverseKGroup(p->next, k);
    while (head != p) {
      ListNode* tmp = head->next;
      head->next = nGroup;
      nGroup = head;
      head = tmp;
    }
    head->next = nGroup;
    return head;
  }
};