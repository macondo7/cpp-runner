#include <vector>

using namespace std;

// Definition for a binary tree node.
struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode() : val(0), left(nullptr), right(nullptr) {}
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
  TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
 public:
  void traversal(TreeNode *root, vector<int> &result, int depth) {
    if (!root) return;
    if (result.size() == depth) result.push_back(root->val);
    traversal(root->right, result, depth + 1);
    traversal(root->left, result, depth + 1);
  }

  vector<int> rightSideView(TreeNode *root) {
    vector<int> result;
    traversal(root, result, 0);
    return result;
  }
};