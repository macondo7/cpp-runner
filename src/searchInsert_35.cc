#include <iostream>
#include <vector>

using namespace std;

class Solution {
 public:
  int searchInsert(vector<int>& nums, int target) {
    int l = 0, r = nums.size() - 1;
    while (l <= r) {
      int mid = l + (r - l) / 2;
      if (nums[mid] < target) {
        l = mid + 1;
      } else {
        r = mid - 1;
      }
    }
    return l;
  }
};

int main() {
  Solution c;
  vector<int> nums = {1};
  cout << c.searchInsert(nums, 2) << endl;
  nums = {1, 2, 3, 4};
  cout << c.searchInsert(nums, 2) << endl;
  nums = {1, 2, 3, 8};
  cout << c.searchInsert(nums, 5) << endl;
}