#include <iostream>
#include <vector>

using namespace std;

class Solution {
  vector<vector<string>> result;
  int n;

 public:
  bool illegal(vector<string> inner, int x, int y) {
    // top illegal
    for (int i = x - 1; i >= 0; i--) {
      if (inner[i][y] == 'Q') return true;
    }
    // top left illegal
    for (int i = x - 1, j = y - 1; i >= 0 && j >= 0; i--, j--) {
      if (inner[i][j] == 'Q') return true;
    }
    // top right illegal
    for (int i = x - 1, j = y + 1; i >= 0 && j < n; i--, j++) {
      if (inner[i][j] == 'Q') return true;
    }
    return false;
  }

  void backtrack(vector<string> inner, int row) {
    if (row == n) {
      result.push_back(inner);
      return;
    }
    for (int i = 0; i < n; i++) {
      if (illegal(inner, row, i)) continue;
      inner[row][i] = 'Q';
      backtrack(inner, row + 1);
      inner[row][i] = '.';
    }
  }

  vector<vector<string>> solveNQueens(int n) {
    this->n = n;
    vector<string> inner(n, string(n, '.'));
    backtrack(inner, 0);
    return result;
  }
};

int main() {
  Solution s;
  vector<vector<string>> result = s.solveNQueens(4);
  for (int i = 0; i < result.size(); i++) {
    for (int j = 0; j < result[i].size(); j++) {
      cout << result[i][j] << ' ';
    }
    cout << endl;
  }
}