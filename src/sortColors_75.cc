#include <iostream>
#include <vector>

using namespace std;

class Solution {
 public:
  void sortColors(vector<int>& nums) {
    int n = nums.size(), red = 0, blue = nums.size() - 1;
    for (int i = 0; i <= blue;) {
      if (nums[i] == 0) {
        swap(nums[i], nums[red]);
        i++;
        red++;
      } else if (nums[i] == 2) {
        swap(nums[i], nums[blue]);
        blue--;
      } else {
        i++;
      }
    }
  }
};