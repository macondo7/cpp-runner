
// Definition for singly-linked list.
struct ListNode {
  int val;
  ListNode* next;
  ListNode() : val(0), next(nullptr) {}
  ListNode(int x) : val(x), next(nullptr) {}
  ListNode(int x, ListNode* next) : val(x), next(next) {}
};

class Solution {
 public:
  ListNode* sortList(ListNode* head) {
    if (!head || !head->next) return head;
    ListNode dummy = ListNode(-1, head);
    ListNode *fast = &dummy, *slow = &dummy;
    while (fast && fast->next) {
      fast = fast->next->next;
      slow = slow->next;
    }
    fast = slow->next;
    slow->next = nullptr;

    ListNode* sorted1 = sortList(dummy.next);
    ListNode* sorted2 = sortList(fast);
    dummy = ListNode(-1);
    ListNode* p = &dummy;
    while (sorted1 && sorted2) {
      if (sorted1->val <= sorted2->val) {
        p->next = sorted1;
        sorted1 = sorted1->next;
      } else {
        p->next = sorted2;
        sorted2 = sorted2->next;
      }
      p = p->next;
    }
    p->next = sorted1 ? sorted1 : sorted2;
    return dummy.next;
  }
};