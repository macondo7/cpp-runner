#include <iostream>
#include <vector>

using namespace std;

// Definition for a binary tree node.
struct TreeNode {
  int val;
  TreeNode *left;
  TreeNode *right;
  TreeNode() : val(0), left(nullptr), right(nullptr) {}
  TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
  TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
};

class Solution {
 public:
  TreeNode *traversal(vector<int> &nums, int l, int r) {
    if (l > r) return nullptr;
    int mid = l + (r - l) / 2;
    TreeNode *left = traversal(nums, l, mid - 1);
    TreeNode *right = traversal(nums, mid + 1, r);
    return new TreeNode(nums[mid], left, right);
  }

  TreeNode *sortedArrayToBST(vector<int> &nums) { return traversal(nums, 0, nums.size() - 1); }
};