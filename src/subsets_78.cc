#include <iostream>
#include <vector>

using namespace std;

class Solution {
  vector<vector<int>> result;

 public:
  void backtrack(vector<int>& nums, vector<int> cur, int idx) {
    result.push_back(cur);
    if (idx == nums.size()) return;
    for (int i = idx; i < nums.size(); i++) {
      cur.push_back(nums[i]);
      backtrack(nums, cur, i + 1);
      cur.pop_back();
    }
  }

  vector<vector<int>> subsets(vector<int>& nums) {
    backtrack(nums, {}, 0);
    return result;
  }
};

int main() {
  Solution c;
  vector<int> input = {1, 2, 3};
  vector<vector<int>> result = c.subsets(input);
  for (vector<int> vars : result) {
    for (int v : vars) {
      cout << v << ' ';
    }
    cout << endl;
  }
}