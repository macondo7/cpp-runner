#include <iostream>
#include <queue>
#include <unordered_map>
#include <vector>

using namespace std;

class Solution {
 public:
  vector<int> topKFrequent(vector<int>& nums, int k) {
    unordered_map<int, int> cnt;
    for (int i = 0; i < nums.size(); i++) {
      cnt[nums[i]]++;
    }
    using pii = pair<int, int>;
    priority_queue<pii, vector<pii>, greater<pii>> pq;
    for (auto& n : cnt) {
      pq.emplace(n.second, n.first);
      if (pq.size() > k) pq.pop();
    }

    vector<int> result;
    while (!pq.empty()) {
      result.push_back(pq.top().second);
      pq.pop();
    }
    return result;
  }
};