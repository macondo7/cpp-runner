#include <iostream>
#include <vector>

using namespace std;

/**
 * Your Trie object will be instantiated and called as such:
 * Trie* obj = new Trie();
 * obj->insert(word);
 * bool param_2 = obj->search(word);
 * bool param_3 = obj->startsWith(prefix);
 */
class Trie {
  vector<Trie*> children;
  bool isEnd;

  Trie* searchWithPrefix(string prefix) {
    Trie* node = this;
    for (char c : prefix) {
      int idx = c - 'a';
      if (!node->children[idx]) return nullptr;
      node = node->children[idx];
    }
    return node;
  }

 public:
  Trie() : children(26), isEnd(false) {}

  void insert(string word) {
    Trie* node = this;
    for (char c : word) {
      int idx = c - 'a';
      if (!node->children[idx]) node->children[idx] = new Trie();
      node = node->children[idx];
    }
    node->isEnd = true;
  }

  bool search(string word) {
    Trie* node = searchWithPrefix(word);
    return node && node->isEnd;
  }

  bool startsWith(string prefix) { return searchWithPrefix(prefix); }
};
