#include <iostream>
#include <vector>

using namespace std;

class Solution {
 public:
  bool wordBreak(string s, vector<string>& wordDict) {
    int n = s.size(), m = wordDict.size();
    vector<bool> dp(n + 1);
    dp[0] = true;
    for (int i = 1; i <= n; i++) {
      for (int j = 0; j < m; j++) {
        int subSize = wordDict[j].size();
        if (i >= subSize && dp[i - subSize]) {
          if (wordDict[j].compare(s.substr(i - subSize, subSize)) != 0) continue;
          dp[i] = true;
          break;
        }
      }
    }
    return dp[n];
  }
};